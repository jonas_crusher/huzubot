require 'cinch'

module Huzubot
    class Bot 
        def initialize
            @bot = Cinch::Bot.new do


                configure do |c|
                    c.server = "huzutech.irc.grove.io"
                    c.port = 6697
                    c.realname = "Johnny Turbo"
                    c.channel = "#Huzuchat"
                    c.nick = "huzubot"
                    c.user = "huzubot"
                    c.ssl.use = true
                    c.password = "Huzutech"
                    #
                    c.verbose = true
                    c.plugins.plugins = [Identify]
                end
            end

            @bot.start
        end
    end

    class Identify
        include Cinch::Plugin

        listen_to :connect, method: :identify
        def identify(m)
            User("NickServ").send "identify !huzubot11!"
        end
    end
end
